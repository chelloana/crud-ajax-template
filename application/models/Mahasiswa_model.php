<?php

    class mahasiswa_model extends CI_model {

        public function getAllmahasiswa()
        {
            $this->db->select('datamahasiswa.namamahasiswa,datamahasiswa.nohp,datamahasiswa.jeniskelamin,datamahasiswa.alamat,datajurusan.idjurusan,datajurusan.jurusan');
            $this->db->from('datamahasiswa');
            $this->db->join('datajurusan','datamahasiswa.idjurusan = datajurusan.idjurusan');
            return $query = $this->db->get()->result_array();
        }


        function ambildata(){
            //return $this->db->get('datamahasiswa');
            $this->db->select('datamahasiswa.idmahasiswa,datamahasiswa.nohp,datamahasiswa.namamahasiswa,datamahasiswa.jeniskelamin,datamahasiswa.alamat,datajurusan.idjurusan,datajurusan.jurusan');
            $this->db->from('datamahasiswa');
            $this->db->join('datajurusan','datamahasiswa.idjurusan = datajurusan.idjurusan');
            return $query = $this->db->get();
        }

        public function tambahdata($data, $table)
        {
            $this->db->insert($table, $data);
        }

        public function ambilidmahasiswa($table, $where)
        {
            return $this->db->get_where($table, $where);
        }

        public function updatedata($where,$data,$table)
        {
            $this->db->where($where);
            $this->db->update($table,$data);
        }

        public function ambiliddetail($table, $where)
        {
            return $this->db->get_where($table, $where);
        }


        public function hapusdata($where,$table)
        {
            $this->db->where($where);
            $this->db->delete($table);
        }

    }

?>