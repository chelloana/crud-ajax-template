<?php

    class Jurusan_model extends CI_model {

        public function getAllJurusan()
        {
            return $query = $this->db->get('datajurusan')->result_array();
        }

        function ambildata(){
            return $this->db->get('datajurusan');
        }

        public function tambahdata($data, $table)
        {
            $this->db->insert($table, $data);
        }

        public function ambilidjurusan($table, $where)
        {
            return $this->db->get_where($table, $where);
        }

        public function updatedata($where,$data,$table)
        {
            $this->db->where($where);
            $this->db->update($table,$data);
        }

        public function hapusdata($where,$table)
        {
            $this->db->where($where);
            $this->db->delete($table);
        }

    }

?>