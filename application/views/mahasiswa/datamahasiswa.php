<div class="content-wrapper">

    <section class="content-header">
      <h1>
        Data Mahasiswa
      </h1>
    </section>

    <br>

    <div class="row">
        <div class="col-md-6">
            <a href="#form" data-toggle="modal" class="btn btn-primary" onclick="submit('tambah')">Tambah Data Mahasiswa</a>
            <a class= "btn btn-warning" href=" <?php echo base_url('Mahasiswa/print') ?>"><i class="fa fa-print"></i>Print</a>
            <div class="dropdown inline">
                <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-download"></i>Export
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="<?php echo base_url('Mahasiswa/pdf') ?>">PDF</a></li>
                    <li><a href="<?php echo base_url('Mahasiswa/excel') ?>">Excel</a></li>
                </ul>
            </div>
        </div>
    </div>

    <br>

    <table class="table table-striped table-bordered data" id="table1">

    

        <thead class="thead-dark">
            <tr>
                <th scope="col">Nama Mahasiswa</th>
                <th scope="col">No Telepon</th>
                <th scope="col">Jenis Kelamin</th>
                <th scope="col">Alamat</th>
                <th scope="col">Jurusan</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>

        <tbody id="target">

        
        </tbody>

    </table>


<div class="modal fade" id="form" role="dialog">
<div class = "modal-dialog">
        <div class = "modal-content">

            <div class="modal-header">
                <h1>Data Mahasiswa</h1>
            </div>

            <p id="pesan"></p>

            <table class="table">
            <tr>
                    <td>Nama Mahasiswa</td>
                    <td><input type="text" name="namamahasiswa" class="form-control">
                    <input type="hidden" name="idmahasiswa" value="">
                    </td>
                </tr>

                <tr>
                    <td>No Telepon</td>
                    <td><input type="text" name="nohp" class="form-control">
                    <input type="hidden" name="idmahasiswa" value="">
                    </td>
                </tr>


                <tr>
                <div class="form-group">
                        <td><label for="jeniskelamin">Jenis Kelamin</label></td>
                        <td><select name="jeniskelamin" class="form-control">
                            <option selected value="Laki-Laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select></td>
                    </div>
                </tr>

                <tr>
                    <td>Alamat</td>
                    <td><input type="text" name="alamat" class="form-control">
                    
                    </td>
                </tr>

                <tr>
                <div class="form-group">
                        <td><label for="idjurusan">Jurusan</label>
                        
                        </td>
                        <td><select name="idjurusan" class="form-control">
                        <?php foreach($jurusan as $jur):?>
                            <option value="<?php echo $jur['idjurusan'];?>"><?php echo $jur['jurusan'];?></option>
                            <?php endforeach;?>
                        </select></td>
                    </div></tr>


                <tr><td></td><td><button type="button" id="btn-tambah" onclick="tambahdata()" class="btn btn-success">Tambah</button>
                <button type="button" id="btn-ubah" onclick="ubahdata()" class="btn btn-success">Edit</button>
                <button type="button" data-dismiss="modal"  id="Cancel" class="btn btn-warning">Cancel</button></td></tr>
</table>

    </div>

    </div>
    </div>
    <div class="modal fade" id="detail" role="dialog">
<div class = "modal-dialog">
        <div class = "modal-content">

            <div class="modal-header">
                <h1>Detail Data Mahasiswa</h1>
            </div>

            <table class="table">
            <tr>
                    <td>Nama Mahasiswa</td>
                    <td><input type="text" name="namamahasiswa" class="form-control" readonly>
                    <input type="hidden" name="idmahasiswa" value="">
                    </td>
            </tr>

            <tr>
                    <td>No Telepon</td>
                    <td><input type="text" name="nohp" class="form-control" readonly>
                    <input type="hidden" name="idmahasiswa" value="">
                    </td>
            </tr>

            <tr>
                    <td>Jenis Kelamin</td>
                    <td><input type="text" name="jeniskelamin" class="form-control" readonly>
                    <input type="hidden" name="idmahasiswa" value="">
                    </td>
            </tr>

                <tr>
                    <td>Alamat</td>
                    <td><input type="text" name="alamat" class="form-control" readonly>
                    
                    </td>
                </tr>

                <tr>
                    <td>Jurusan</td>
                    <!-- <td><input type="text" name="jurusan" class="form-control" readonly> -->
                    <td><input type="text" value="<?php echo $jur['jurusan'];?>" class="form-control" readonly>
                    </td>
                </tr>

                <tr><td>
                    <button type="button" data-dismiss="modal"  id="Back" class="btn btn-warning">Back</button>
                </td></tr>
            </table>
        </div>
    </div>
</div>

    <script type="text/javascript">
    ambilData();
            function ambilData(){
                $.ajax({
                    type : 'POST',
                    url : '<?php echo site_url()."Mahasiswa/ambildata" ?>',
                    dataType : 'json',
                    success : function(data){
                        //console.log(data);
                        var baris = '';
                        // for(var i=0;i<data.lenght;i++){
                        //   console.log(data[i]);
                        //   baris += '<tr>'+
                        //         '<td>' + data[i].idjurusan + '</td>'+
                        //         '<td>' + data[i].jurusan + '</td>'+
                        //     '</tr>';
                        // }
                        data.forEach(function(val) {
                           //console.log(val.jurusan); 
                           baris += '<tr>'+
                                '<td>' + val.namamahasiswa + '</td>'+
                                '<td>' + val.nohp + '</td>'+
                                '<td>' + val.jeniskelamin + '</td>'+
                                '<td>' + val.alamat + '</td>'+
                                '<td>' + val.jurusan + '</td>' +
                                '<td>'+
                                    '<a href="#form" data-toggle="modal" class="btn btn-primary" onclick="submit('+val.idmahasiswa+')">Edit</a>'+' '+
                                    '<a href="#detail" data-toggle="modal" class="btn btn-warning" onclick="detailsub('+val.idmahasiswa+')">Detail</a>'+' '+
                                    '<a onclick="hapusdata('+val.idmahasiswa+')" class="btn btn-danger">Hapus</a>'+
                                '</td>' 
                            '</tr>';
                        });
                        $('#target').html(baris);
                    }
                });
            }

function tambahdata(){
    var namamahasiswa=$("[name = 'namamahasiswa']").val();
    var nohp=$("[name = 'nohp']").val();
    var jeniskelamin=$("[name = 'jeniskelamin']").val();
    var alamat=$("[name = 'alamat']").val();
    var idjurusan=$("[name = 'idjurusan']").val();


    $.ajax({
        type : 'POST',
        data : 'namamahasiswa='+namamahasiswa+'&nohp='+nohp+'&jeniskelamin='+jeniskelamin+'&alamat='+alamat+'&idjurusan='+idjurusan,
        url  : '<?php echo base_url()."Mahasiswa/tambahdata" ?>',
        dataType : 'json',
        success : function(hasil){
            $("#pesan").html(hasil.pesan);

        if(hasil.pesan==''){
            $("#form").modal('hide');
            ambilData();

            $("[name='namamahasiswa']").val('');
            $("[name='nohp']").val('');
            $("[name='jeniskelamin']").val('');
            $("[name='alamat']").val('');
            $("[name='idjurusan']").val('');

        }

        }
    });
}

function submit(x){
    if(x=='tambah'){
        $('#btn-tambah').show();
        $('#btn-ubah').hide();
    }else{
        $('#btn-tambah').hide();
        $('#btn-ubah').show();

        $.ajax({
            type : 'POST',
            data : 'idmahasiswa='+x,
            url  : '<?php echo base_url()."Mahasiswa/ambilidmahasiswa" ?>',
            dataType : 'json',
            success : function(hasil){
                $('[name="idmahasiswa"]').val(hasil[0].idmahasiswa);
                $('[name="namamahasiswa"]').val(hasil[0].namamahasiswa);
                $('[name="nohp"]').val(hasil[0].nohp);
                $('[name="jeniskelamin"]').val(hasil[0].jeniskelamin);
                $('[name="alamat"]').val(hasil[0].alamat);
                $('[name="idjurusan"]').val(hasil[0].idjurusan);
            }
        });
    }
}


function ubahdata(){
    var idmahasiswa=$("[name='idmahasiswa']").val();
    var namamahasiswa=$("[name='namamahasiswa']").val();
    var nohp=$("[name='nohp']").val();
    var jeniskelamin=$("[name='jeniskelamin']").val();
    var alamat=$("[name='alamat']").val();
    var idjurusan=$("[name='idjurusan']").val();
    $.ajax({
        type : 'POST',
        data : 'idmahasiswa='+idmahasiswa+'&nohp='+nohp+'&namamahasiswa='+namamahasiswa+'&jeniskelamin='+jeniskelamin+'&alamat='+alamat+'&idjurusan='+idjurusan,
        url  : '<?php echo base_url().'Mahasiswa/ubahdata' ?>',
        dataType : 'json',
        success : function(hasil){
            $("#pesan").html(hasil.pesan);

            if(hasil.pesan==''){
                $("#form").modal('hide');
                ambilData();
            }

        }
    })
}

function detailsub(x){

        $.ajax({
            type : 'POST',
            data : 'idmahasiswa='+x,
            url  : '<?php echo base_url()."Mahasiswa/ambiliddetail" ?>',
            dataType : 'json',
            success : function(hasil){
                $('[name="idmahasiswa"]').val(hasil[0].idmahasiswa);
                $('[name="namamahasiswa"]').val(hasil[0].namamahasiswa);
                $('[name="nohp"]').val(hasil[0].nohp);
                $('[name="jeniskelamin"]').val(hasil[0].jeniskelamin);
                $('[name="alamat"]').val(hasil[0].alamat);
                $('[name="jurusan"]').val(hasil[0].jurusan);

            }
        });
    }



function hapusdata(idmahasiswa){
    var tanya= confirm('Apakah anda yakin akan menghapus data?');

    if(tanya){
        $.ajax({
            type : 'POST',
            data : 'idmahasiswa='+idmahasiswa,
            url  : '<?php echo base_url()."Mahasiswa/hapusdata" ?>',
            //dataType : 'json',
            success : function(){
                ambilData();
            }
        })
    }
}
</script>