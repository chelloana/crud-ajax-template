<div class="divcontainer">

    <div class="divrow mt-3">
        <div class="col-md-6">

        <div class="card">
            <div class="card-header">
                Form Tambah Data Mahasiswa
            </div>

            <div class="card-body">

                <form action="<?= base_url(); ?>Relasi/simpan" method="post">

                    <div class="form-group">
                        <label for="namamahasiswa">Nama Mahasiswa</label>
                        <input type="text" name="namamahasiswa" class="form-control" id="namamahasiswa">
                    </div>

                    <div class="form-group">
                        <label for="jeniskelamin">Jenis Kelamin</label>
                        <select name="jeniskelamin" class="form-control">
                            <option selected value="Laki">Laki-Laki</option>
                            <option value="Peremuan">Perempuan</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" name="alamat" class="form-control" id="alamat">
                    </div>

                    <div class="form-group">
                        <label for="idjurusan">Jurusan</label>
                        <select name="idjurusan" class="form-control">
                        <?php foreach($jurusan as $jur):?>
                            <option value="<?php echo $jur['idjurusan'];?>"><?php echo $jur['jurusan'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>

                    <button type="submit" name="tambah" class="btn btn-success">Tambah</button>

                </form>

            </div>
        </div>
        
           
        </div>
    </div>

</div>