<div class="divcontainer">

    <div class="divrow mt-3">
        <div class="col-md-6">

        <div class="card">
            <div class="card-header">
                Form Edit Data Mahasiswa
            </div>

            <?php foreach ($relasi as $rls) { ?>

            <div class="card-body">


                <form action="<?= base_url().'Relasi/ubah' ?>" method="post">

                    <div class="form-group">
                        <label for="namamahasiswa">Nama Mahasiswa</label>
                        <input type="hidden" name="idmahasiswa" class="form-control" value="<?= $rls->idmahasiswa ?>">
                        <input type="text" name="namamahasiswa" class="form-control" value="<?= $rls->namamahasiswa ?>" id="namamahasiswa">
                    </div>

                    <div class="form-group">
                        <label for="jeniskelamin">Jenis Kelamin</label>                       
                        <select name="jeniskelamin" class="form-control">

                                <option value="<?= $rls->jeniskelamin?>"
                                    <?php $kondisi = ($rls->jeniskelamin);
                                        if ($kondisi == "L"){
                                            echo "selected";
                                        }
                                    
                                ?>> Laki-Laki </option>

                                <option value="<?= $rls->jeniskelamin?>"
                                    <?php $kondisi = ($rls->jeniskelamin);
                                        if ($kondisi == "P"){
                                            echo "selected";
                                        }
                                    
                                ?>> Perempuan </option>

                                
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        
                        <input type="text" name="alamat" value="<?= $rls->alamat ?>" class="form-control" id="alamat">
                    </div>

                    <div class="form-group">
                        <label for="idjurusan">Jurusan</label>
                        
                        <select name="idjurusan" class="form-control">
                        <?php foreach($jurusan as $jur):?>
                            <option value="<?php echo $jur['idjurusan'];?>"  

                                <?php $kondisi = ($rls->idjurusan);
                                    if ($kondisi == $jur['idjurusan']){
                                        echo "selected";
                                    }  
                                ?> >

                            <?php echo $jur['jurusan'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>

                    <button type="submit" name="tambah" class="btn btn-success">Edit</button>

                </form>

            <?php } ?>


            </div>
        </div>
        
           
        </div>
    </div>

</div>