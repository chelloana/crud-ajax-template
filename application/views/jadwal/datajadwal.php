<div class="content-wrapper">

    <section class="content-header">
      <h1>
        Jadwal
      </h1>
    </section>

    <br>

    <div class="row">
        <div class="col-md-6">
            <a href="#form" data-toggle="modal" class="btn btn-primary" onclick="submit('tambah')">Tambah Jadwal</a>
            <a class= "btn btn-warning" href=" <?php echo base_url('Jadwal/print') ?>"><i class="fa fa-print"></i>Print</a>

            <div class="dropdown inline">
                <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-download"></i>Export
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="<?php echo base_url('Jadwal/pdf') ?>">PDF</a></li>
                    <li><a href="<?php echo base_url('Jadwal/excel') ?>">Excel</a></li>
                </ul>
            </div>
        </div>
    </div>

    <br>

    <table class="table table-striped table-bordered data" id="table1">

    

        <thead class="thead-dark">
            <tr>
                <th scope="col">Semester</th>
                <th scope="col">Mata Kuliah</th>
                <th scope="col">Jam Mulai</th>
                <th scope="col">Jam Selesai</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>

        <tbody id="target">

        
        </tbody>

    </table>


<div class="modal fade" id="form" role="dialog">
<div class = "modal-dialog">
        <div class = "modal-content">

            <div class="modal-header">
                <h1>Jadwal</h1>
            </div>

            <p id="pesan"></p>

            <table class="table">
            <tr>
                <div class="form-group">
                        <td><label for="idsemester">Semester</label>
                        
                        </td>
                        <td><select name="idsemester" class="form-control">
                        <?php foreach($semester as $jur):?>
                            <option value="<?php echo $jur['idsemester'];?>"><?php echo $jur['namasemester'];?></option>
                            <?php endforeach;?>
                        </select></td>
                </div>
            </tr>
            <tr>
                <div class="form-group">
                        <td><label for="idmatkul">Mata Kuliah</label>
                        
                        </td>
                        <td><select name="idmatkul" class="form-control">
                        <?php foreach($matkul as $jur):?>
                            <option value="<?php echo $jur['idmatkul'];?>"><?php echo $jur['namamatkul'];?></option>
                            <?php endforeach;?>
                        </select></td>
                </div>
            </tr>

            <tr>
                    <td>Jam Mulai</td>
                    <td><input type="time" name="jam" class="form-control">
                    <input type="hidden" name="idjadwal" value="">
                    </td>
            </tr>

            <tr>
                    <td>Jam Selesai</td>
                    <td><input type="time" name="jamselesai" class="form-control">
                    <input type="hidden" name="idjadwal" value="">
                    </td>
            </tr>

            <tr>
                    <td>Tanggal</td>
                    <td><input type="date" name="tanggal" class="form-control">
                    <input type="hidden" name="idjadwal" value="">
                    </td>
            </tr>

                <tr><td></td><td><button type="button" id="btn-tambah" onclick="tambahdata()" class="btn btn-success">Tambah</button>
                <button type="button" id="btn-ubah" onclick="ubahdata()" class="btn btn-success">Edit</button>
                <button type="button" data-dismiss="modal"  id="Cancel" class="btn btn-warning">Cancel</button></td></tr>
</table>

    </div>

    </div>
    </div>
    <!-- <div class="modal fade" id="detail" role="dialog">
<div class = "modal-dialog">
        <div class = "modal-content">

            <div class="modal-header">
                <h1>Detail Data Mata Kuliah</h1>
            </div>

            <table class="table">
            <tr>
                    <td>ID Mata Kuliah</td>
                    <td><input type="text" name="idmatkul" class="form-control" readonly>
                    <input type="hidden" name="idmatkul" value="">
                    </td>
            </tr>

            <tr>
                    <td>Nama Mata Kuliah</td>
                    <td><input type="text" name="namamatkul" class="form-control" readonly>
                    <input type="hidden" name="idmatkul" value="">
                    </td>
            </tr>

                <tr>
                    <td>ID Semester</td>
                    <td><input type="text" name="idmatkul" class="form-control" readonly>
                    
                    </td>
                </tr>

                <tr><td>
                    <button type="button" data-dismiss="modal"  id="Back" class="btn btn-warning">Back</button>
                </td></tr>
            </table>
        </div>
    </div>
</div> -->

    <script type="text/javascript">
    ambilData();
            function ambilData(){
                $.ajax({
                    type : 'POST',
                    url : '<?php echo site_url()."Jadwal/ambildata" ?>',
                    dataType : 'json',
                    success : function(data){
                        //console.log(data);
                        var baris = '';
                        // for(var i=0;i<data.lenght;i++){
                        //   console.log(data[i]);
                        //   baris += '<tr>'+
                        //         '<td>' + data[i].idjurusan + '</td>'+
                        //         '<td>' + data[i].jurusan + '</td>'+
                        //     '</tr>';
                        // }
                        data.forEach(function(val) {
                           //console.log(val.jurusan); 
                           baris += '<tr>'+
                                '<td>' + val.namasemester + '</td>'+
                                '<td>' + val.namamatkul + '</td>'+
                                '<td>' + val.jam + '</td>'+
                                '<td>' + val.jamselesai + '</td>'+
                                '<td>' + val.tanggal + '</td>'+
                                '<td>'+
                                    '<a href="#form" data-toggle="modal" class="btn btn-primary" onclick="submit('+val.idjadwal+')">Edit</a>'+' '+
                                    '<a onclick="hapusdata('+val.idjadwal+')" class="btn btn-danger">Hapus</a>'+
                                '</td>' 
                            '</tr>';
                        });
                        $('#target').html(baris);
                    }
                });
            }

function tambahdata(){
    var idsemester=$("[name = 'idsemester']").val();
    var idmatkul=$("[name = 'idmatkul']").val();
    var jam=$("[name = 'jam']").val();
    var jamselesai=$("[name = 'jamselesai']").val();
    var tanggal=$("[name = 'tanggal']").val();

    $.ajax({
        type : 'POST',
        data : 'idsemester='+idsemester+'&idmatkul='+idmatkul+'&jam='+jam+'&jamselesai='+jamselesai+'&tanggal='+tanggal,
        url  : '<?php echo base_url()."Jadwal/tambahdata" ?>',
        dataType : 'json',
        success : function(hasil){
            $("#pesan").html(hasil.pesan);

        if(hasil.pesan==''){
            $("#form").modal('hide');
            ambilData();

            $("[name='idsemester']").val('');
            $("[name='idmatkul']").val('');
            $("[name='jam']").val('');
            $("[name='tanggal']").val('');

        }

        }
    });
}

function submit(x){
    if(x=='tambah'){
        $('#btn-tambah').show();
        $('#btn-ubah').hide();
    }else{
        $('#btn-tambah').hide();
        $('#btn-ubah').show();

        $.ajax({
            type : 'POST',
            data : 'idjadwal='+x,
            url  : '<?php echo base_url()."Jadwal/ambilidjadwal" ?>',
            dataType : 'json',
            success : function(hasil){
                $('[name="idjadwal"]').val(hasil[0].idjadwal);
                $('[name="idsemester"]').val(hasil[0].idsemester);
                $('[name="idmatkul"]').val(hasil[0].idmatkul);
                $('[name="jam"]').val(hasil[0].jam);
                $('[name="jamselesai"]').val(hasil[0].jamselesai);
                $('[name="tanggal"]').val(hasil[0].tanggal);
            }
        });
    }
}


function ubahdata(){
    var idjadwal=$("[name='idjadwal']").val();
    var idsemester=$("[name = 'idsemester']").val();
    var idmatkul=$("[name = 'idmatkul']").val();
    var jam=$("[name = 'jam']").val();
    var jamselesai=$("[name = 'jamselesai']").val();
    var tanggal=$("[name = 'tanggal']").val();

    $.ajax({
        type : 'POST',
        data : 'idjadwal='+idjadwal+'&idsemester='+idsemester+'&idmatkul='+idmatkul+'&jam='+jam+'&jamselesai='+jamselesai+'&tanggal='+tanggal,
        url  : '<?php echo base_url().'Jadwal/ubahdata' ?>',
        dataType : 'json',
        success : function(hasil){
            $("#pesan").html(hasil.pesan);

            if(hasil.pesan==''){
                $("#form").modal('hide');
                ambilData();
            }

        }
    })
}

// function detailsub(x){

//         $.ajax({
//             type : 'POST',
//             data : 'idmatkul='+x,
//             url  : '<?php echo base_url()."Matkul/ambiliddetail" ?>',
//             dataType : 'json',
//             success : function(hasil){
//                 $('[name="idmatkul"]').val(hasil[0].idmatkul);
//                 $('[name="namamatkul"]').val(hasil[0].namamatkul);
//                 $('[name="idsemester"]').val(hasil[0].idsemester);
//             }
//         });
//     }



function hapusdata(idjadwal){
    var tanya= confirm('Apakah anda yakin akan menghapus data?');

    if(tanya){
        $.ajax({
            type : 'POST',
            data : 'idjadwal='+idjadwal,
            url  : '<?php echo base_url()."Jadwal/hapusdata" ?>',
            //dataType : 'json',
            success : function(){
                ambilData();
            }
        })
    }
}
</script>