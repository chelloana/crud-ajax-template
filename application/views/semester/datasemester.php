<div class="content-wrapper">

    <section class="content-header">
      <h1>
       Semester
      </h1>
    </section>


    <br>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <a href="#form" data-toggle="modal" class="btn btn-primary" onclick="submit('tambah')">Tambah Semester</a>
            <a class= "btn btn-warning" href=" <?php echo base_url('Semester/print') ?>"><i class="fa fa-print"></i>Print</a>

            <div class="dropdown inline">
                <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-download"></i>Export
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="<?php echo base_url('Semester/pdf') ?>">PDF</a></li>
                    <li><a href="<?php echo base_url('Semester/excel') ?>">Excel</a></li>
                </ul>
            </div>
       </div>
    </div>

    <br>

    <table class="table table-striped table-bordered data" id="table1">

    

        <thead class="thead-dark">
            <tr>
                <th scope="col">ID Semester</th>
                <th scope="col">Semester</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>

        <tbody id="target">

        
        </tbody>

    </table>

<div class="modal fade" id="form" role="dialog">
<div class = "modal-dialog">
        <div class = "modal-content">

            <div class="modal-header">
                <h1>Semester</h1>
            </div>

            <p id="pesan"></p>

            <table class ="table">
                <tr>
                    <td>Semester</td>
                    <td><input type="text" name="namasemester" class="form-control">
                        <input type="hidden" name="idsemester" value="">
                    </td>
                </tr>

                <tr><td></td><td><button type="button" id="btn-tambah" onclick="tambahdata()" class="btn btn-success">Tambah</button>
                <button type="button" id="btn-ubah" onclick="ubahdata()" class="btn btn-success">Edit</button>
                <button type="button" data-dismiss="modal"  id="Cancel" class="btn btn-warning">Cancel</button></td></tr>
</table>

    </div>

    </section>
    <script type="text/javascript">
    ambilData();
            function ambilData(){
                $.ajax({
                    type : 'POST',
                    url : '<?php echo site_url()."semester/ambildata" ?>',
                    dataType : 'json',
                    success : function(data){
                            //console.log(data);
                            var baris = '';
                        // for(var i=0;i<data.lenght;i++){
                        //   console.log(data[i]);
                        //   baris += '<tr>'+
                        //         '<td>' + data[i].idjurusan + '</td>'+
                        //         '<td>' + data[i].jurusan + '</td>'+
                        //     '</tr>';
                        // }
                        data.forEach(function(val) {
                           //console.log(val.jurusan); 
                           baris += '<tr>'+
                                '<td>' + val.idsemester + '</td>'+
                                '<td>' + val.namasemester + '</td>'+
                                '<td>'+
                                    '<a href="#form" data-toggle="modal" class="btn btn-primary" onclick="submit('+val.idsemester+')">Edit</a>'+' '+
                                    '<a onclick="hapusdata('+val.idsemester+')" class="btn btn-danger">Hapus</a>'+
                                '</td>' 
                            '</tr>';
                        });
                        $('#target').html(baris);
                    }
                });
            }

function tambahdata(){
    var namasemester=$("[name = 'namasemester']").val();

    $.ajax({
        type : 'POST',
        data : 'namasemester='+namasemester,
        url  : '<?php echo base_url()."semester/tambahdata" ?>',
        dataType : 'json',
        success : function(hasil){
            $("#pesan").html(hasil.pesan);

        if(hasil.pesan==''){
            $("#form").modal('hide');
            ambilData();

            $("[name='namasemester']").val('');
        }

        }
    });
}

function submit(x){
    if(x=='tambah'){
        $('#btn-tambah').show();
        $('#btn-ubah').hide();
    }else{
        $('#btn-tambah').hide();
        $('#btn-ubah').show();

        $.ajax({
            type : 'POST',
            data : 'idsemester='+x,
            url  : '<?php echo base_url()."semester/ambilidsemester" ?>',
            dataType : 'json',
            success : function(hasil){
                $('[name="idsemester"]').val(hasil[0].idsemester);
                $('[name="namasemester"]').val(hasil[0].namasemester);

            }
        });
    }
}

function ubahdata(){
    var idsemester=$("[name = 'idsemester']").val();
    var namasemester=$("[name = 'namasemester']").val();

    $.ajax({
        type : 'POST',
        data : 'idsemester='+idsemester+'&namasemester='+namasemester,
        url  : '<?php echo base_url()."semester/ubahdata" ?>',
        dataType : 'json',
        success : function(hasil){
            $("#pesan").html(hasil.pesan);

        if(hasil.pesan==''){
            $("#form").modal('hide');
            ambilData();

        }
}
    });

}

function hapusdata(idsemester)
{
    var tanya= confirm('Apakah anda yakin akan menghapus data?');

    if(tanya){
        $.ajax({
            type : 'POST',
            data : 'idsemester='+idsemester,
            url  : '<?php echo base_url()."semester/hapusdata" ?>',
            //dataType : 'json',
            success : function(){
                ambilData();
            }
        });
    }
}

</script>
    