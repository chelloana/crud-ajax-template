<div class="divcontainer">

    <div class="divrow mt-3">
        <div class="col-md-6">

        <div class="card">
            <div class="card-header">
                Form Edit Data Jurusan
            </div>

            <?php foreach ($jurusan as $jrs) { ?>

            <div class="card-body">


                <form action="<?= base_url().'Jurusan/ubah' ?>" method="post">

                    <div class="form-group">
                        <label for="namaurusan">Nama Jurusan</label>
                        <input type="hidden" name="idjurusan" class="form-control" value="<?= $jrs->idjurusan ?>">
                        <input type="text" name="jurusan" class="form-control" value="<?= $jrs->jurusan ?>" id="namajurusan">
                    </div>

                    <button type="submit" name="tambah" class="btn btn-success">Edit</button>

                </form>

            <?php } ?>


            </div>
        </div>
        
           
        </div>
    </div>

</div>