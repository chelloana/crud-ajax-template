<?php

    class Login extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            $this->load->model('login_model', 'l');  
            $this->load->helper('form'); 
            $this->load->helper('url');
        }

        public function index()
        {
            $data['judul'] = 'Login';
            $data['login'] = $this->l->getAllLogin();
            $this->load->view('login/datalogin', $data);
        }

        public function ceklogin(){
            $namalogin = $this->input->post('namalogin');
            $passlogin = $this->input->post('passlogin');
            $this->load->model('Login_model');
            $this->l->ambillogin($namalogin,$passlogin);
           }

        public function logout(){
        $this->session->set_userdata('namalogin', FALSE);
        $this->session->sess_destroy();
        redirect('login');
            }
    }
?>