<?php

    class Jurusan extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            $this->load->model('jurusan_model', 'm');  
            $this->load->helper('form'); 
            $this->load->helper('url');
        }

        public function index()
        {
            $data['judul'] = 'Data Jurusan';
            $data['jurusan'] = $this->m->getAllJurusan();
            $this->load->view('templates2/header', $data);
            $this->load->view('templates2/sidebar');
            $this->load->view('jurusan/datajurusan');
            $this->load->view('templates2/footer');
        }

        function ambildata(){
            $datajurusan = $this->m->ambildata('datajurusan')->result_array();
            echo json_encode($datajurusan);
        }

        function tambahdata(){
            $jurusan = $this->input->post('jurusan');

            if($jurusan==''){
                $result['pesan']="Nama jurusan harus diisi";
            }else{
                $result['pesan']="";

            $data=array(
                'jurusan' => $jurusan
            );

            $this->m->tambahdata($data,'datajurusan');

            }

            echo json_encode($result);
        }

        public function ambilidjurusan()
        {
            $idjurusan=$this->input->post('idjurusan');
            $where=array('idjurusan' => $idjurusan);
            $datajurusan = $this->m->ambilidjurusan('datajurusan', $where)->result();

            echo json_encode($datajurusan);
        }

        public function ubahdata()
        {
            $idjurusan = $this->input->post('idjurusan');
            $jurusan = $this->input->post('jurusan');

            if($jurusan==''){
                $result['pesan']="Nama jurusan harus diisi";
            }else{
                $result['pesan']="";

            $where=array('idjurusan'=>$idjurusan);

            $data=array(
                'jurusan' => $jurusan
            );

            $this->m->updatedata($where,$data,'datajurusan');

            }

            echo json_encode($result);
        }

        public function hapusdata()
        {
            $idjurusan=$this->input->post('idjurusan');
            $where=array('idjurusan'=>$idjurusan);

            $this->m->hapusdata($where,'datajurusan');
        }

        public function print(){
            $data['jurusan'] = $this->m->ambildata("datajurusan")->result();
            $this->load->view('jurusan/printjurusan', $data);
        }

        public function pdf(){
            $this->load->library('dompdf_gen');

            $data['jurusan'] = $this->m->ambildata("datajurusan")->result();
            $this->load->view('jurusan/pdf', $data);

            $paper_size = 'A4';
            $orientation = 'landscape';
            $html = $this->output->get_output();
            $this->dompdf->set_paper($paper_size, $orientation);

            $this->dompdf->load_html($html);
            $this->dompdf->render();
            $this->dompdf->stream("data_jurusan.pdf", array('Attachement' =>0));
        }

        public function excel(){
            $data['jurusan'] = $this->m->ambildata("datajurusan")->result();

            require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
            require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');
    
    // Panggil class PHPExcel nya
    $excel = new PHPExcel();
    // Settingan awal fil excel
    $excel->getProperties()->setCreator('Data Jurusan')
                 ->setLastModifiedBy('Data Jurusan')
                 ->setTitle("Data Jurusan")
                 ->setSubject("Data Jurusan")
                 ->setDescription("Data Jurusan")
                 ->setKeywords("Data Jurusan");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );


    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
    $objDrawing->setCoordinates('A1');
    $objDrawing->setName('univ');
    $objDrawing->setDescription('univ');
    $objDrawing->setPath('asset/gambar/univ.png');
    $objDrawing->setWidth(100)->setHeight(100);


    $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA JURUSAN"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "No"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "Nama"); // Set kolom B3 dengan tulisan "NIS"

    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);

    // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
    //$jurusan = $this->m->view('jurusan/datajurusan');
    //$no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach($data['jurusan'] as $jrs){// Lakukan looping pada variabel siswa

      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $jrs->idjurusan);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $jrs->jurusan);

      
      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
    //   $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
    //   $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
    //   $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      
      //$no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
    // $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
    // $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
    // $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Data Jurusan");
    $excel->setActiveSheetIndex(0);
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Jurusan.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');

        }

    }