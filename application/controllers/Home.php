<?php

    class Home extends CI_Controller {

        public function index()
        {
            $data['judul'] = 'Home';
            $this->load->view('templates2/header', $data);
            $this->load->view('templates2/sidebar');
            $this->load->view('home/datahome');
            $this->load->view('templates2/footer');
        }

        public function getData()
        {
            $this->load->model('home_model', 'm');
            $data = $this->m->getAllHome();
            echo json_encode($data);
            // print_r($cek);
            // exit();
        }

        public function getDataa()
        {
            $this->load->model('home_model', 'm');
            $data = $this->m->getAllHomee();
            echo json_encode($data);
        }
    }