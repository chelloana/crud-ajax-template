#BUILD THE IMAGE
docker build -t awc dockerfile/

#RUN AT PORT 8016
docker run -i -t -p "8016:80" -v ${PWD}/../:/app -v ${PWD}/db/mysql:/var/lib/mysql --name awc awc:latest

#detach mode
docker run -d -p "8016:80" -v ${PWD}/../:/app -v ${PWD}/db/mysql:/var/lib/mysql --name awc awc:latest